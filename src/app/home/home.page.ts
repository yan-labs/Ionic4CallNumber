import { Component } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private callNumber: CallNumber
  ) {}

  callNow(number: string) {
    this.callNumber.callNumber(number, true)
      .then(response => {
        console.log('Launched dialer!', response);
      })
      .catch(err => {
        console.log('Error launched dialer', err);
      });
  }

}
